package controllers

import (
	"fmt"
	"freamework/pkg/Router"
)

func Exemple(c *Router.Context) {
	_, err := fmt.Fprintf(c.Response, "Hello Word")
	if err != nil {
		return
	}
}

func PostExemple(c *Router.Context, data *Router.PostData) {
	fmt.Println(data)
}
