package main

import (
	"freamework/routes"
	"net/http"
)

func main() {
	router := routes.Api()

	// Crie um servidor HTTP usando o roteador.
	err := http.ListenAndServe(":8080", router)
	if err != nil {
		return
	}
}
