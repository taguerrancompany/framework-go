package Router

import (
	"encoding/json"
	"net/http"
)

// Context é uma estrutura personalizada que encapsula http.ResponseWriter e *http.Request.
type Context struct {
	Response http.ResponseWriter
	Request  *http.Request
}
type PostData struct {
	Message string `json:"message"`
}

// Handler é uma função que lida com solicitações HTTP.
type Handler func(*Context)

type Router struct {
	routes map[string]map[string]Handler
}

func NewRouter() *Router {
	return &Router{
		routes: make(map[string]map[string]Handler),
	}

}

func (r *Router) GET(path string, handler Handler) {
	r.addRoute("GET", path, handler)
}

func (r *Router) POST(path string, handler func(*Context, *PostData)) {
	r.addRoute("POST", path, func(c *Context) {
		// Decodifica o JSON do corpo da solicitação em uma estrutura PostData.
		var postData PostData
		err := json.NewDecoder(c.Request.Body).Decode(&postData)
		if err != nil {
			http.Error(c.Response, "Erro na decodificação JSON", http.StatusBadRequest)
			return
		}

		// Chama o manipulador do controlador passando o contexto e os dados tratados.
		handler(c, &postData)
	})
}

func (r *Router) PUT(path string, handler Handler) {
	r.addRoute("PUT", path, handler)
}

func (r *Router) DELETE(path string, handler Handler) {
	r.addRoute("DELETE", path, handler)
}
func (r *Router) addRoute(method, path string, handler Handler) {
	if r.routes[method] == nil {
		r.routes[method] = make(map[string]Handler)
	}
	r.routes[method][path] = handler

}

// ServeHTTP implementa a interface http.Handler para o roteador.
func (r *Router) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	method := req.Method
	path := req.URL.Path
	handlerMap, ok := r.routes[method]
	if ok {
		handler, ok := handlerMap[path]
		if ok {
			context := &Context{Response: w, Request: req}
			handler(context)
			return
		}
	}
	http.NotFound(w, req)
}
