package routes

import (
	"freamework/App/http/controllers"
	"freamework/pkg/Router"
)

func Api() *Router.Router {
	api := Router.NewRouter()

	api.GET("/", func(c *Router.Context) {
		controllers.Exemple(c)
	})
	api.POST("/exeple", func(context *Router.Context, data *Router.PostData) {
		controllers.PostExemple(context, data)
	})

	return api
}
